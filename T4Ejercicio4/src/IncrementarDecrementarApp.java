
public class IncrementarDecrementarApp {

	public static void main(String[] args) {
		int N = 24;
		
		int incrementar = N + 77;
		int decrementar = N - 3;
		int duplicar = N * 2;
		
		System.out.println("Valor Inicial = " + N);
		System.out.println("Incremento: " + incrementar);
		System.out.println("Decremento: " + decrementar);
		System.out.println("Doble: " + duplicar);

	}

}
